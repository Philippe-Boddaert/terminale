# Cours NSI

## Découpage

<table cellspacing="0" cellpadding="0" style="border-collapse: collapse;margin:auto;" >
<body>
<tr>
<td style="text-align: center;"><a title="Représentation des données" href="./structures"><img src='./assets/list.svg' width="128px"/><br /><img src='./assets/heap.svg' width="128px"/><br /><img src='./assets/tree.svg' width="128px"/><br/>Structures de données</a></td>
<td style="border: none;text-align: center;"><a title="Base de données" href="./bdd"><img src='./assets/database.svg' width="128px"/><br/>Base de données</a></td>
<td style="border: none;text-align: center;"><a title="Architectures matérielles et systèmes d'exploitation" href="./architecture"><img src='./assets/system.svg' width="128px"/><br/>Architectures matérielles, systèmes d'exploitation et Réseau</a></td>
<td style="border: none;text-align: center;"><a title="Langages et programmation" href="./programmation"><img src='./assets/python.svg' width="128px"/><br/>Langages et programmation</a></td>
<td style="border: none;text-align: center;"><a title="Algorithmique" href="./algorithmique"><img src='./assets/algorithm.svg' width="128px"/><br/>Algorithmique</a></td>
</tr>
</body>
</table>
## Progression

| Trimestre | Objectifs | Séquence 1               | Séquence 2                 |
| :--: | :-- | :-- | :-- |
| 1 | - __Coder__ différemment, de manière plus lisible et maintenable<br />- __Manipuler__ de nouveaux __types de données__ pour résoudre des problèmes | **Langage de programmation** :<br />  - Modularité<br />  - Gestion de bugs<br />  - Programmation orienté objet<br />  - Récursivité | **Structures de données** : <br />  - Liste chainée<br />  - Pile<br />  - File<br />  - Dictionnaire |
| 2 | - __Organiser__ un ensemble de données possiblement __volumineux__<br />- __Analyser__ l'exécution de programme par un __OS__<br />- __Transmettre__ une information dans un __réseau__ | **Structures de données** : <br />  - Arbre binaire et ABR<br/><br/>**Algorithmique** : <br />  - Algorithmes sur les arbres<br />  - Méthode « diviser pour régner » | **Base de données relationnelles** :<br/>    - SGBD<br/>    - Modèle relationnel<br/>    - SQL<br/><br/> **Architectures matérielles, systèmes d'exploitation et Réseau** : <br />  - Processus et ordonnancement<br />  - Protocoles de routage<br />  - Définition d'un SOC |
| 3 | - __Sécuriser__ une communication<br/>- __Étudier__ des __algorithmes__ avancés<br/>- __Limiter__ le champ de la discipline : __Peut-on tout programmer ?__ | **Structures de données** : <br />  - Graphe<br/><br/>**Algorithmique** :<br />  - Algorithmes sur les graphes<br/> - Programmation dynamique<br/> - Recherche textuelle | **Langage de programmation** :<br /> - Calculabilité<br/> - Paradigmes de programmation<br /><br />**Architectures matérielles, systèmes d'exploitation et Réseau**<br />  - Sécurisation des communications |

## Sitographie

- [Gitlab de David Landry](https://gitlab.com/david_landry/nsi)
- [Framagit de Christophe Mieszczak](https://framagit.org/tofmzk/informatique_git/-/tree/master/premiere_nsi)

